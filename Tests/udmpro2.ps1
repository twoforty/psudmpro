[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [string]$Url,

    [pscredential]$Credential
)

if (Get-Module PSUdmPro) {
    Remove-Module PSUdmPro
}
Import-Module ./PSUdmPro.psm1

Initialize-UdmPro -Url $Url -SkipCertificateCheck
Connect-UdmPro -Credential $Credential -Debug

foreach($fwd in (Get-PortForward | Where-Object { $_.pfwd_interface -ne "both"})) {
    Write-Debug "Processing [$($fwd.name)] with id [$($fwd._id)]"
    Set-PortForward -Id $fwd._id -Interface "both"
}
