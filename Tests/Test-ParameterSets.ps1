[CmdletBinding(DefaultParameterSetName="None")]
param (
    [Parameter(ParameterSetName="A")]
    [string]$A,
    [Parameter(ParameterSetName="B")]
    [string]$B
)

Write-Output $PSCmdlet.ParameterSetName

Write-Output "A=$A"
Write-Output "B=$B"
