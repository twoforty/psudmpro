[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [string]$Url,

    [pscredential]$Credential
)

if (Get-Module PSUdmPro) {
    Remove-Module PSUdmPro
}
Import-Module ./PSUdmPro.psm1

Initialize-UdmPro -Url $Url -SkipCertificateCheck
Connect-UdmPro -Credential $Credential -Debug

Get-APGroups
(Get-APGroups | Measure-Object).Count
