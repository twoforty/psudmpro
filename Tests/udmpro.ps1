[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [string]$Url,

    [pscredential]$Credential
)

if (Get-Module PSUdmPro) {
    Remove-Module PSUdmPro
}
Import-Module ./PSUdmPro.psm1

Initialize-UdmPro -Url $Url -SkipCertificateCheck
Connect-UdmPro -Credential $Credential -Debug

# PSUdmPro\Get-UserSelf
Write-Debug "Rules"
$rules = PSUdmPro\Get-TrafficRule -Debug
$rules
Write-Debug "Filter it"
$rule = $rules | Where-Object { $_.description -eq "TEST" }
if ($rule) {
    $rule
    $rule._id
    Write-Debug "Get it directly"
    PSUdmPro\Get-TrafficRule -Id $rule._id -Debug
    PSUdmPro\Remove-TrafficRule -Id $rule._id -Debug
} else {
    Write-Debug "Rule not found"
}

$global:rule = $rules | ? { $_.description -eq "TEST"}
# PSUdmPro\Get-Token

$ips = @(
    @{
        ip_or_subnet = "127.0.0.1"
        ports = @(
            39015
        )
        port_ranges = @()
        ip_version = "v4"
    }
)
# PSUdmPro\Add-TrafficRule -Description "TEST" -TargetType "CLIENT" -TargetMac "18:c0:4d:0c:b7:4b" -IPAddresses $ips
