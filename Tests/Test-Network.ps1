[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [string]$Url,

    [pscredential]$Credential
)

$VerbosePreference = 'continue'
$DebugPreference = 'Continue'

if (Get-Module PSUdmPro) {
    Write-Verbose "Remove-Module"
    Remove-Module PSUdmPro
}
Write-Verbose "Import-Module"
Import-Module ./PSUdmPro.psm1

Write-Verbose "Initialize and connect"
Initialize-UdmPro -Url $Url -SkipCertificateCheck
Connect-UdmPro -Credential $Credential -Debug

Write-Verbose "List networks"
Get-Network -Debug

Write-Verbose "Find Network 'TEST'"
$id = (Get-Network | Where-Object { $_.name -eq "TEST" })._id
if ($id) {
    Write-Verbose "Remove-Network 'TEST' via id=$id"
    Remove-Network -Id $id -Debug
}

# Write-Verbose "Remove-Network 'TEST2' via name"
# Remove-Network -Name "TEST2"
