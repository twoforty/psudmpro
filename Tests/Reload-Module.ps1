if (Get-Module PSUdmPro) {
    Remove-Module PSUdmPro
}
Import-Module ./PSUdmPro.psm1

if ($url -and $cred) {
    Initialize-UdmPro -Url $url -SkipCertificateCheck
    Connect-UdmPro -Credential $cred -Debug
}
