[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [string]$Url,

    [pscredential]$Credential
)

$global:VerbosePreference = 'continue'
$global:DebugPreference = 'continue'

if (Get-Module PSUdmPro) {
    Remove-Module PSUdmPro
}
Import-Module ./PSUdmPro.psm1

Initialize-UdmPro -Url $Url -SkipCertificateCheck
Connect-UdmPro -Credential $Credential -Debug

Get-EthernetProfile
try {
    Write-Output "Remove EthernetProfile 'TEST5'"
    Remove-EthernetProfile -Name "TEST5"
} catch {
    Write-Output "EthernetProfile 'TEST5' does not exist"
}
Write-Output "Add EthernetProfile 'TEST5'"
Add-EthernetProfile -Name "TEST5"

Write-Output "Add EthernetProfile 'TEST6'"
Add-EthernetProfile -Name "TEST6" -NativeNetwork "VLAN-CLIENTS"

Add-EthernetProfile -Name "TEST7" -NativeNetwork "VLAN-INFRA" -PortIsolation -EgressRateLimit 2000 -VoiceNetwork "VLAN-PARTY" -LinkSpeed 2500 -MulticastStormControl
