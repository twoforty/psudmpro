[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [string]$Url,

    [pscredential]$Credential
)

if (Get-Module PSUdmPro) {
    Remove-Module PSUdmPro
}
Import-Module ./PSUdmPro.psm1

Initialize-UdmPro -Url $Url -SkipCertificateCheck
Connect-UdmPro -Credential $Credential -Debug

Get-WiFiNetwork -Debug -Verbose

Write-Verbose "Find WiFi network by name 'TEST'"
Get-WiFiNetwork | Where-Object { $_.name -eq "TEST" }

Remove-WiFiNetwork -Name "TEST"
