function Get-EthernetProfile {
    <#
    .SYNOPSIS
    Get ethernet port profiles.

    .LINK
    Add-EthernetProfile

    .LINK
    Remove-EthernetProfile
    #>

    [CmdletBinding()]
    param (
        [string]$Name
    )

    $result = Invoke-GetUdmPro -Path "rest/portconf"
    if ($Name) {
        $result = $result | Where-Object { $_.name -like $Name }
    }

    return $result
}
