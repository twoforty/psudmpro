function Add-Network {
    <#
    .SYNOPSIS
    Adds a new Virtual Network.

    .LINK
    Get-Network

    .LINK
    Remove-Network

    .LINK
    Set-Network

    .NOTES
    This cmdlet supports only IPv4.
    #>

    [CmdletBinding()]
    param (
        # Network Name
        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Name,

        # Disable LTE Internet backup.
        [switch]$DisableLTEBackup,

        # Specify "default" to use DM Pro as the router or a switch name or MAC address.
        [string]$Router = "default",

        # Gateway IP/Subnet
        [string]$HostAddress,

        # Mask
        [ValidateRange(8,30)]
        [int]$Mask = 24,

        # Disable the network.
        [switch]$Disabled,

# -- Advanced
        [int]$VlanId,

        [switch]$GuestNetwork,

        [switch]$IsolateNetwork,

        [switch]$DisableInternetAccess,

        [ValidateSet("none", "work", "family")]
        [string]$ContentFiltering = "none",

        # TODO: IGMP Snooping
        # TODO: Multicast DNS

# -- DHCP
        [ValidateSet("none", "server", "relay")]
        [string]$DhcpMode = "server",
        [string]$DhcpStart,
        [string]$DhcpStop,
        [switch]$DhcpGuarding,
        [string[]]$TrustedDhcpServers,

        [string]$DhcpDomainName,

        [ValidateCount(1, 4)]
        [string[]]$DhcpDnsServers,

        [int]$DhcpLeasetime = 86400
    )

    # TODO: Figure out how DhcpMode works. For now, only "server" is supported.
    if ($DhcpMode -ne "server") {
        throw "Only server is supported for DhcpMode"
    }

    # If HostAddress is specified, then AutoScale will be disabled.
    if ($HostAddress) {
        # No Auto-Scale
        $autoScale = $false
    } else {
        # Auto-Scale
        $autoScale = $true
        $subnets = Get-SubnetSuggestions
        $gateway = ($subnets | Where-Object { $_.mask -eq $Mask }).gateways | Select-Object -First 1
        if ($null -eq $gateway) {
            throw "No available subnet found"
        }
        $DhcpStart = $gateway -replace "\d+$","6"
        $DhcpStop = $gateway -replace "\d+$","254"
        $ipSubnet = ("{0}/{1}" -f $gateway, $Mask)
        $vlan = $gateway -replace "\d+\.\d+\.(\d+)\.\d+",'$1'
    }

    if ($GuestNetwork) {
        $purpose = "guest"
    } else {
        $purpose = "corporate"
    }

    $body = @{
        dhcpd_start = $DhcpStart
        dhcpd_stop = $DhcpStop
        enabled = -not $Disabled
        ip_subnet = $ipSubnet
        name = $Name
        vlan = $vlan
        vlan_enabled = $true
        auto_scale_enabled = $autoScale
        dhcp_relay_enabled = $false
        dhcpd_boot_enabled = $false
        dhcpd_dns_enabled = $false
        dhcpd_enabled = $true
        dhcpd_gateway_enabled = $false
        dhcpd_leasetime = $DhcpLeasetime
        dhcpd_ntp_enabled = $false
        dhcpd_tftp_server = ""
        dhcpd_time_offset_enabled = $false
        dhcpd_unifi_controller = ""
        dhcpd_wpad_url = ""
        dhcpguard_enabled = $DhcpGuarding
        domain_name = $DhcpDomainName
        igmp_proxy_downstream = $false
        igmp_snooping = $false
        ipv6_interface_type = "none"
        ipv6_ra_enabled = $true
        ipv6_setting_preference = "auto"
        lte_lan_enabled = -not $DisableLTEBackup
        mdns_enabled = $true
        networkgroup = "LAN"
        purpose = $purpose
        setting_preference = "auto"
        upnp_lan_enabled = $false
        dhcpdv6_allow_slaac = $true
        ipv6_pd_auto_prefixid_enabled = $true
        nat_outbound_ip_addresses = @()
        internet_access_enabled = -not $DisableInternetAccess
        gateway_type = "default"
    }

    if ($Router -ne "default") {
        $body.gateway_type = "switch"
        $body.gateway_device = $Router
        $device = Get-Device -Name $Router
        if ($device -and $device.PSObject.Properties.name -contains("mac")) {
            $body.gateway_device = $device.mac
        }
    }

    if ($DhcpDnsServers) {
        $body.dhcpd_dns_enabled = $false
        $index = 1
        foreach($ip in $DhcpDnsServers) {
            $key = "dhcpd_dns_$index"
            $body.$key = $ip
            # TODO: Validate $ip as a valid IP.
        }
    }

    if ($DhcpGuarding) {
        foreach($ip in $TrustedDhcpServers) {
            $key = "dhcpd_ip_$index"
            $body.$key = $ip
            # TODO: Validate $ip as a valid IP.
        }
    }

    return Invoke-PostUdmPro -Path "rest/networkconf" -Body $body
}
