function Get-SubnetSuggestions {
    <#
    .SYNOPSIS
    Get suggested networks.

    .LINK
    Add-Network

    .LINK
    Add-AutoScaleNetwork
    #>

    return Invoke-GetUdmPro -Path "stat/widget/subnet-suggest"

}
