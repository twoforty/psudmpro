function Remove-WiFiNetwork {
    <#
    .SYNOPSIS
    Removes an existing WiFi Network

    .PARAMETER Id
    WiFi Network Id.

    .PARAMETER Name
    WiFi Network Name.

    .LINK
    Get-WiFiNetwork

    .LINK
    Add-WiFiNetwork

    .LINK
    Set-WiFiNetwork
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory, ParameterSetName="id")]
        [ValidateNotNullOrEmpty()]
        [string]$Id,

        [Parameter(Mandatory, ParameterSetName="name")]
        [ValidateNotNullOrEmpty()]
        [string]$Name
    )

    if ($PSCmdlet.ParameterSetName -eq "name") {
        $network = Get-WiFiNetwork | Where-Object { $_.name -eq $Name }
        if (!$network) {
            throw "No WiFi Network found with name $Name"
        }
        $Id = $network._id
    }

    # Failsafe
    if ([string]::IsNullOrWhiteSpace($Id)) {
        throw "Id is empty; internal error"
    }

    Invoke-DeleteUdmPro -Path "rest/wlanconf/$Id"
}
