function Add-FirewallGroup {
    <#
    .SYNOPSIS
    Add a new firewall or IP group.

    .LINK
    Get-FirewallGroup

    .LINK
    Set-FirewallGroup

    .LINK
    Remove-FirewallGroup
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Name,

        [ValidateSet("port-group", "address-group", "ipv6-address-group")]
        [string]$Type = "port-group",

        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string[]]$Members
    )

    $body = @{
        name = $Name
        group_type = $Type
        group_members = $Members
    }

    # if type == port-group => should we parse all possible values?
    # What are possible values? => Siblge port number, range "1-10", comma separated list?
    # if type == address-group => should we parse all possible values?
    # What are possible values? => IP address, CIDR, other? => CIDR doesn't work.
    # Tested values:
    #   - 192.168.1.1
    #   - 192.168.2.0
    #   - 192.168.3.10-192.168.3.20
    # if type == ipv6-address-group => should we parse all possible values?
    # What are possible values?

    return Invoke-PostUdmPro -Path "rest/firewallgroup" -Body $body
}

