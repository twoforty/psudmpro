function Remove-Network {
    <#
    .SYNOPSIS
    Removes an existing network

    .PARAMETER Id
    Network Id.

    .PARAMETER Name
    Network Name.

    .LINK
    Get-Network

    .LINK
    Add-Network

    .LINK
    Set-Network

    .LINK
    Get-NetworkSettings
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory, ParameterSetName="id")]
        [ValidateNotNullOrEmpty()]
        [string]$Id,

        [Parameter(Mandatory, ParameterSetName="name")]
        [ValidateNotNullOrEmpty()]
        [string]$Name
    )

    switch($PSCmdlet.ParameterSetName) {
        "id" {
            $network = Get-Network | Where-Object { $_._id -eq $Id }
            if (!$network) {
                throw "Network with id $Id not found"
            }
            $Name = $network.name
        }

        "name" {
            $network = Get-Network | Where-Object { $_.name -eq $Name }
            if (!$network) {
                throw "Network with name $Name not found"
            }
            $Id = $network._id
        }
    }

    $body = @{
        name = $Name
    }

    return Invoke-DeleteUdmPro -Path "rest/networkconf/$Id" -Body $body
}
