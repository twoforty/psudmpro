function Add-WiFiNetwork {
    <#
    .SYNOPSIS
    Adds a new Auto-Scale network with default options.

    .PARAMETER Name
    Name of the new Network.

    .PARAMETER Mask
    Size of the network. Choose from 20, 22 or 24 mask network.
    Note: currently only 24 is supported.

    .PARAMETER Disabled
    Turn on if the network should be disabled.

    .PARAMETER DisableLTEBackup
    Turn off U-LTE Internet Backup.

    .PARAMETER Router
    The router that will service the Network.
    Specify "default" for the UDM Pro or the MAC address of a layer 3 switch.

    .LINK
    Get-Network

    .LINK
    Add-Network

    .LINK
    Set-Network

    .LINK
    Remove-Network

    .LINK
    Get-UnifiDevices

    .NOTES
    This cmdlet supports only value 24 for the Mask parameter.
    This cmdlet supports only IPv4.
    #>

    [CmdletBinding()]
    param (
        # Network Name
        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Name,

        # Mask
        [ValidateSet("20", "22", "24")]
        [string]$Mask = "24",

        [switch]$Disabled,

        [switch]$DisableLTEBackup,

        # Specify "default" or a switch MAC address.
        [string]$Router = "default"
    )

    # TODO: For now, only Mask = 24 is supported.
    # I don't yet know what to set dor start/end when other than 24 is selected.

    $subnets = Get-SubnetSuggestions
    $gateway = ($subnets | Where-Object { $_.mask -eq $Mask }).gateways | Select-Object -First 1
    if ($null -eq $gateway) {
        throw "No available subnet found"
    }

    $dhcpdStart = $gateway -replace "\d+$","6"
    $dhcpdStop = $gateway -replace "\d+$","254"
    $ipSubnet = ("{0}/{1}" -f $gateway, $Mask)
    $vlan = $gateway -replace "\d+\.\d+\.(\d+)\.\d+",'$1'
    Write-Verbose "Add-AutoScaleNetwork(Name=$Name)"
    Write-Debug "dhcp start-stop=$dhcpdStart-$dhcpdStop"
    Write-Debug "ip_subnet=$ipSubnet"
    Write-Debug "vlan=$vlan"
    $body = @{
        dhcpd_start = $dhcpdStart
        dhcpd_stop = $dhcpdStop
        enabled = -not $Disabled
        ip_subnet = $ipSubnet
        name = $Name
        vlan = $vlan
        vlan_enabled = true
        auto_scale_enabled = true
        dhcp_relay_enabled = false
        dhcpd_boot_enabled = false
        dhcpd_dns_enabled = false
        dhcpd_enabled = true
        dhcpd_gateway_enabled = false
        dhcpd_leasetime = 86400
        dhcpd_ntp_enabled = false
        dhcpd_tftp_server = ""
        dhcpd_time_offset_enabled = false
        dhcpd_unifi_controller = ""
        dhcpd_wpad_url = ""
        dhcpguard_enabled = false
        domain_name = ""
        igmp_proxy_downstream = false
        igmp_snooping = false
        ipv6_interface_type = "none"
        ipv6_ra_enabled = true
        ipv6_setting_preference = "auto"
        lte_lan_enabled = -not $DisableLTEBackup
        mdns_enabled = true
        networkgroup = "LAN"
        purpose = "corporate"
        setting_preference = "auto"
        upnp_lan_enabled = false
        dhcpdv6_allow_slaac = true
        ipv6_pd_auto_prefixid_enabled = true
        nat_outbound_ip_addresses = @()
        internet_access_enabled = true
        gateway_type = "default"
    }

    if ($Router -ne "default") {
        $body.gateway_type = "switch"
        $body.gateway_device = $Router
    }

    return Invoke-PostUdmPro -Path "rest/networkconf" -Body $body
}
