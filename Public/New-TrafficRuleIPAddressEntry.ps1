function New-TrafficRuleIPAddressEntry {
    <#
    .SYNOPSIS
    Creates a hashtable to be used by Add-TrafficRule.

    .LINK
    Add-TrafficRule
    #>

    [CmdletBinding(DefaultParameterSetName = "Port")]
    param (
        [ValidateNotNullOrWhiteSpace()]
        [string]$IpOrSubnet,

        [Parameter(ParameterSetName="Port")]
        [int[]]$Port = @(),

        [Parameter(ParameterSetName="Portrange")]
        [object[]]$PortRange
    )

    # Validate IpOrSubnet
    $parts = $IpOrSubnet -split '/'
    if ($parts.Count -eq 1) {
        if (![bool]($IpOrSubnet -as [ipaddress])) {
            throw [System.ArgumentException] "Invalid IP address"
        }
    } elseif ($parts.Count -eq 2) {
        if (![bool]($parts[0] -as [ipaddress])) {
            throw [System.ArgumentException] "Invalid IP address"
        }
        if (![bool]($parts[1] -as [int])) {
            throw [System.ArgumentException] "Invalid Subnet address"
        }
        $range = $parts[1] -as [int]
        if ($range -lt 0 -or $range -gt 32) {
            throw [System.ArgumentException] "Invalid Subnet address"
        }
    } else {
        throw [System.ArgumentException] "Invalid IP or Subnet address"
    }

    if (([ipaddress]$IpOrSubnet).AddressFamily -eq "InterNetworkV6") {
        $ipVersion = "v6"
    } else {
        $ipVersion = "v4"
    }
    $entry = @{
        ip_or_subnet = $IpOrSubnet
        ports = $Port
        port_ranges = @()
        ip_version = $ipVersion
    }
    if ($PSCmdlet.ParameterSetName -eq "Portrange") {
        foreach($range in $PortRange) {
            $entry.port_ranges += "@{port_start=$($range.Start); port_stop=$($range.Stop)}"
        }
    }

    return $entry
}
