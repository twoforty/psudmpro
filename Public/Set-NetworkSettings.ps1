function Set-NetworkSettings {
    <#
    .SYNOPSIS
    Get Global Network Settings.

    .LINK
    Set-NetworkSettings
    #>

    [CmdletBinding()]
    param (
        [ValidateSet("on", "off")]
        [string]$IpV6Support,

        [string[]]$AddMdns,
        [string[]]$RemoveMdns,
        [string[]]$SetMdns,

        [string[]]$AddIgmpSnooping,
        [string[]]$RemoveIgmpSnooping,
        [string[]]$SetIgmpSnooping,

        [string[]]$AddIgmpProxy,
        [string[]]$RemoveIgmpProxy,
        [string[]]$SetIgmpProxy
    )

    $settings = Get-NetworkSettings
    if ($IpV6Support) {
        $settings.ipv6_enabled = $IpV6Support -eq "on"
    }

#region mdns
    if ($AddMdns) {
        foreach($item in $AddMdns) {
            # Resolve name to Id
            $network = Get-Network -Name $item
            if ($network) {
                # Found a network by name.
                $id = $network._id
            } else {
                $id = $item
            }
            $settings.mdns_enabled_for_network_ids += $id
        }
    }

    if ($RemoveMdns) {
        foreach($item in $RemoveMdns) {
            # Resolve name to Id
            $network = Get-Network -Name $item
            if ($network) {
                # Found a network by name.
                $id = $network._id
            } else {
                $id = $item
            }
            $settings.mdns_enabled_for_network_ids = `
                $settings.mdns_enabled_for_network_ids | Where-Object { $_ -ne $id }
        }
    }

    if ($SetMdns) {
        $settings.mdns_enabled_for_network_ids = @()
        foreach($item in $SetMdns) {
            # Resolve name to Id
            $network = Get-Network -Name $item
            if ($network) {
                # Found a network by name.
                $id = $network._id
            } else {
                $id = $item
            }
            $settings.mdns_enabled_for_network_ids += $id
        }
    }

    if ($settings.mdns_enabled_for_network_ids.Count -eq 0) {
        $settings.mdns_enabled_for = "none"
    } else {
        $settings.mdns_enabled_for = "some"
    }
#endregion

#region IgmpSnooping
if ($AddIgmpSnooping) {
    foreach($item in $AddIgmpSnooping) {
        # Resolve name to Id
        $network = Get-Network -Name $item
        if ($network) {
            # Found a network by name.
            $id = $network._id
        } else {
            $id = $item
        }
        $settings.igmp_snooping_for_network_ids += $id
    }
}

if ($RemoveIgmpSnooping) {
    foreach($item in $RemoveIgmpSnooping) {
        # Resolve name to Id
        $network = Get-Network -Name $item
        if ($network) {
            # Found a network by name.
            $id = $network._id
        } else {
            $id = $item
        }
        $settings.igmp_snooping_for_network_ids = `
            $settings.igmp_snooping_for_network_ids | Where-Object { $_ -ne $id }
    }
}

if ($SetIgmpSnooping) {
    $settings.igmp_snooping_for_network_ids = @()
    foreach($item in $RemoveIgmpSnooping) {
        # Resolve name to Id
        $network = Get-Network -Name $item
        if ($network) {
            # Found a network by name.
            $id = $network._id
        } else {
            $id = $item
        }
        $settings.igmp_snooping_for_network_ids += $id
    }
}

if ($settings.igmp_snooping_for_network_ids.Count -eq 0) {
    $settings.igmp_snooping_for = "none"
} else {
    $settings.igmp_snooping_for = "some"
}
#endregion

#region IgmpProxy
if ($AddIgmpProxy) {
    foreach($item in $AddIgmpProxy) {
        # Resolve name to Id
        $network = Get-Network -Name $item
        if ($network) {
            # Found a network by name.
            $id = $network._id
        } else {
            $id = $item
        }
        $settings.igmp_proxy_for_network_ids += $id
    }
}

if ($RemoveIgmpProxy) {
    foreach($item in $RemoveIgmpProxy) {
        # Resolve name to Id
        $network = Get-Network -Name $item
        if ($network) {
            # Found a network by name.
            $id = $network._id
        } else {
            $id = $item
        }
        $settings.igmp_proxy_for_network_ids = `
            $settings.igmp_proxy_for_network_ids | Where-Object { $_ -ne $id }
    }
}

if ($SetIgmpProxy) {
    $settings.igmp_proxy_for_network_ids = @()
    foreach($item in $SetIgmpProxy) {
        # Resolve name to Id
        $network = Get-Network -Name $item
        if ($network) {
            # Found a network by name.
            $id = $network._id
        } else {
            $id = $item
        }
        $settings.igmp_proxy_for_network_ids += $id
    }
}

if ($settings.igmp_proxy_for_network_ids.Count -eq 0) {
    $settings.igmp_proxy_for = "none"
} else {
    $settings.igmp_proxy_for = "some"
}
#endregion

# TODO: ipv6_pd_interfaces

    return Invoke-PutUdmPro -Path "global/config/network" -Body $settings
}
