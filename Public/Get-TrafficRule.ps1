function Get-TrafficRule {
    <#
    .SYNOPSIS
    Gets traffic rules.

    .LINK
    Remove-TrafficRule
    #>

    [CmdletBinding()]
    param ()

    Invoke-GetUdmPro -Path "trafficrules"
}
