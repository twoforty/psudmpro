function Get-Backup {
    <#
    .SYNOPSIS
    List backups on the Network Application.

    .LINK
    Add-Backup

    .LINK
    Set-Backup

    .LINK
    Remove-Backup
    #>

    [CmdletBinding()]
    param ()

    Write-Debug "Entry Get-Backup"
    $result = Invoke-GetUdmPro -Path "cmd/backup"
    Write-Debug "Exit Get-Backup"
    return $result
}
