function Get-NetworkSettings {
    <#
    .SYNOPSIS
    Get Global Network Settings.

    .PARAMETER ResolveIds
    Resolves Ids to names. Adds etra return properties containing the resolved names.

    .LINK
    Set-NetworkSettings
    #>

    [CmdletBinding()]
    param (
        [switch]$ResolveIds
    )

    $result = Invoke-GetUdmPro -Path "global/config/network"
    if ($ResolveIds) {
        Write-Verbose "Resolve IGMP snooping Ids"
        $result | Add-Member -Name 'igmp_snooping_for_networks' -Type NoteProperty -Value @()
        foreach($id in $result.igmp_snooping_for_network_ids) {
            $network = Get-Network -Id $id
            if ($network) {
                $result.igmp_snooping_for_networks += $network.name
            }
        }

        Write-Verbose "Resolve MDNS Ids"
        $result | Add-Member -Name 'mdns_enabled_for_networks' -Type NoteProperty -Value @()
        foreach($id in $result.mdns_enabled_for_network_ids) {
            $network = Get-Network -Id $id
            if ($network) {
                $result.mdns_enabled_for_networks += $network.name
            }
        }
    }

    return $result
}
