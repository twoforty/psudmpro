function Remove-FirewallGroup {
    <#
    .SYNOPSIS
    Remove firewall or IP groups.

    .LINK
    Add-FirewallGroup

    .LINK
    Set-FirewallGroup

    .LINK
    Get-FirewallGroup
    #>

    [CmdletBinding(DefaultParameterSetName="id", SupportsShouldProcess)]
    param (
        [Parameter(ParameterSetName="id", ValueFromPipeline)]
        [string]$Id,

        [Parameter(ParameterSetName="name")]
        [string]$Name,

        [Parameter(ParameterSetName="pscustomobject", ValueFromPipeline)]
        [PSCustomObject]$Object
    )

    switch($PSCmdlet.ParameterSetName) {
        "pscustomobject" {
            if ($Object.PSObject.Properties.name -contains("_id")) {
                $Id = $Object._id
            }
        }

        "name" {
            $result = Invoke-GetUdmPro -Path "rest/firewallgroup"
            $fwg = $result | Where-Object { $_.name -eq $Name }
            if (!$fwg) {
                throw "FirewallGroup not found"
            }
            $Id = $fwg._id
        }
    }

    if ($PSCmdlet.ShouldProcess($Id, "Remove Firewallgroup")) {
        return Invoke-DeleteUdmPro -Path "rest/firewallgroup/$Id"
    }
}

