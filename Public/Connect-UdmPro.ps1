function Connect-UdmPro {
    <#
    .SYNOPSIS
    Create a login session with a Unifi Udm Pro.
    #>

    param (
        [Parameter(Mandatory)]
        [pscredential]$Credential
    )
    if (!$Script:baseUrl) {
        throw "Call Initialize-PSUdmPro first"
    }

    $url = "$baseUrl/api/auth/login"
    $body = @{
        username = $Credential.UserName
        password = $Credential.GetNetworkCredential().Password
        token = ""
        rememberMe = $false
    }
    $arguments = @{
        Uri = $url
        Method = "POST"
        Body = ($body | ConvertTo-Json)
        ContentType = "application/json"
        SkipCertificateCheck = $Script:SkipCertificateCheck
    }
    $responseHeaders = $null
    $Script:userSelf = Invoke-Restmethod @arguments -ResponseHeadersVariable responseHeaders
    $cookieParts = ($responseHeaders["Set-Cookie"] -split ";" | ForEach-Object { $_.Trim() })
    # [0] = TOKEN=xyz
    # [1] = path=/
    # [2] = expires=Mon, 19 Feb 2024 01:51:46 GMT
    # [3] = samesite=none
    # [4] = secure
    # [5] = httponly
    $tokenName, $tokenValue = $cookieParts[0] -split "="
    $cookie = new-object System.Net.Cookie
    $cookie.name = $tokenName
    $cookie.value = $tokenValue
    $cookie.domain = ([System.Uri]$Script:baseUrl).Host
    #$cookie.expires = "01/10/2026 09:15 AM"
    $Script:webSession = New-Object Microsoft.PowerShell.Commands.WebRequestSession
    $Script:webSession.cookies.add($cookie)

    # Parse the csrf token from the TOKEN response.
    $Script:token = $tokenValue
    $tokenJsonPart = ($tokenValue -split '\.')[1]
    try {
        $tokenJsonPart = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($tokenJsonPart))
    } catch {
        try {
            $tokenJsonPart = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($tokenJsonPart + "="))
        } catch {
            $tokenJsonPart = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($tokenJsonPart + "=="))
        }
    }
    $Script:tokenJsonPart = ($tokenJsonPart | ConvertFrom-Json)
    # $Script:tokenJsonPart contains:
    # {"userId":"xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx","passwordRevision":0,"isRemembered":false,"csrfToken":"xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx","iat":1708310038,"exp":1708317238,"jti":"xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"}
    # Write-Debug $Script:tokenJsonPart.csrfToken
}
