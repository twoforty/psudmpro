function Add-EthernetProfile {
    <#
    .SYNOPSIS
    Get ethernet port profiles.

    .LINK
    Get-EthernetProfile

    .LINK
    Remove-EthernetProfile
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Name,

        [ValidateSet("auto", "disabled", "restricted")]
        [string]$Port = "auto",

        # When $Port == "restricted", specify restricted MAC Ids.
        [string[]]$RestrictedMac = @(),

        # Must be the name of the Network / VLAN
        [string]$NativeNetwork = "Default",

        [ValidateSet("allow_all", "auto", "block_all", "custom")]
        # "auto" is the same as "allow_all"
        [string]$TaggedVlanManagement = "allow_all",

        [ValidateSet("auto", "off")]
        [string]$PoeMode = "auto",

        # When LinkSpeed is NOT given a value, Auto Negotiate is enabled.
        # Valid options:
        # 10 = 10Mbps
        # 100 = 100Mbps
        # 1000 = 1Gbps
        # 2500 = 2.5Gbps
        # 5000 = 5Gbps
        # 10000 = 10Gbps
        # 20000 = 20Gbps
        # 25000 = 25Gbps
        # 40000 = 40Gbps
        # 50000 = 50Gbps
        [ValidateSet(10, 100, 1000, 2500, 5000, 10000, 20000, 250000, 40000, 50000)]
        [int]$LinkSpeed,
        [switch]$HalfDuplex,

        # 802.1X Control
        [ValidateSet("force_authorized", "force_unauthorized", "auto", "mac_based", "multi_host")]
        [string]$Dot1xControl = "force_authorized",

        # Only valid when Dot1xControl == mac_based.
        [int]$Dot1xIdleTimeout = 300,

        # Egress Rate Limit in Kbps. When > zero, it will be turned on.
        # 2Mbps = 2000
        [ValidateRange("NonNegative")]
        [int]$EgressRateLimit,

        # Default disabled/off
        [switch]$PortIsolation,

        # The next parameters together make the "Storm Control" option.
        [switch]$UnicastStormControl,
        [switch]$MulticastStormControl,
        [switch]$BroadcastStormControl,

        # Default disabled/off
        [switch]$LoopProtection,

        # Voice VLAN
        [string]$VoiceNetwork
    )

    if ($Port -eq "disabled") {
        Write-Debug "Port is set to diabled"
        # When Port is disabled, the following options get disabled in the GUI:
        # - Native VLAN / Network
        # - Tagged VLAN Management
        # - Advanced settings

        $nativeNetworkId = ""
        $TaggedVlanManagement = "block_all"
        $port_security_enabled = $true
        $RestrictedMac = @()
    } else {
        if ($Port -eq "restricted") {
            $port_security_enabled = $true
        } else {
            $port_security_enabled = $false
        }

        # Transalate option.
        if ($TaggedVlanManagement -eq "allow_all") {
            $TaggedVlanManagement = "auto"
        }

        # Lookup Native Network Id.
        Write-Verbose "Resolve NativeNetwork '$NativeNetwork' to it's id"
        $nativeNetworkId = (Get-Network -Name $NativeNetwork)._id
        Write-Debug "NativeNetwork '$NativeNetwork' resolved to '$nativeNetworkId'"

        if (![string]::IsNullOrWhiteSpace($VoiceNetwork)) {
            Write-Verbose "Resolve '$VoiceNetwork' to it's id"
            # $VoiceNetwork could be an id or a name. Lookup name to resolve id.
            $network = (Get-Network -Name $VoiceNetwork)
            if ($network) {
                Write-Debug "VoiceNetwork '$VoiceNetwork' resolved to '$($network._id)'"
                $VoiceNetwork = $network._id
            }
        }
    }

    # If any of the "manual" properties have been specified,
    # be sure to change setting_preference from "auto" to "manual".
    # If not, the "manual" values are ignored.
    $settingsPreference = "auto"
    if ($PortIsolation.IsPresent -or
        $LoopProtection.IsPresent -or
        $VoiceNetwork -or
        $EgressRateLimit -or
        $LinkSpeed -or
        $UnicastStormControl.IsPresent -or
        $MulticastStormControl.IsPresent -or
        $BroadcastStormControl.IsPresent) {
        $settingsPreference = "manual"
    }

    $body = @{
        name = $Name
        newMacAddress = ""
        op_mode = "switch"
        poe_mode = $PoeMode
        port_security_enabled = $port_security_enabled
        port_security_mac_address = $RestrictedMac
        native_networkconf_id = $nativeNetworkId
        tagged_vlan_mgmt = $TaggedVlanManagement
        setting_preference = $settingsPreference
        dot1x_ctrl = $Dot1xControl
        dot1x_idle_timeout = $Dot1xIdleTimeout
        voice_networkconf_id = ""
        stp_port_mode = $true
        lldpmed_enabled = $true
        autoneg = $true
        egress_rate_limit_kbps_enabled = $false
        isolation = $PortIsolation.IsPresent
        lldpmed_notify_enabled = $false
        port_keepalive_enabled = $LoopProtection.IsPresent
        stormctrl_bcast_enabled = $BroadcastStormControl.IsPresent
        stormctrl_mcast_enabled = $MulticastStormControl.IsPresent
        stormctrl_ucast_enabled = $UnicastStormControl.IsPresent
        excluded_networkconf_ids = @()
    }

    if ($Port -ne "disabled") {
        # If LinkSpeed is specified, disable auto negotiation and specify link speed.
        if ($LinkSpeed) {
            $body.autoneg = $false
            $body.speed = $LinkSpeed
            $body.full_duplex = -not $HalfDuplex
        }

        if ($EgressRateLimit -eq 0) {
            $body.egress_rate_limit_kbps_enabled = $false
        } else {
            $body.egress_rate_limit_kbps_enabled = $true
            $body.egress_rate_limit_kbps = $EgressRateLimit
        }
    }

    Write-Verbose ($body | ConvertTo-Json | Out-String)
    Invoke-PostUdmPro -Path "rest/portconf" -Body $body
}
