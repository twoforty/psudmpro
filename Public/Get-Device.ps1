function Get-Device {
    <#
    .SYNOPSIS
    Get devices or device.
    #>

    [CmdletBinding()]
    param (
        [string]$Name,

        [switch]$Full
    )

    Write-Debug "Entry Get-Device"

    $url = "stat/device-basic"
    $result = Invoke-GetUdmPro -Path $url
    if ($Name) {
        # Some devices do not have a name property, e.g. USMINI.
        $newResult = @()
        foreach($item in $result) {
            if ($item.PSObject.Properties.name -contains("name")) {
                if ($item.name -like $Name) {
                    $newResult += $item
                }
            }
        }
        $result = $newResult
    }
    Write-Debug "Exit Get-Device"
    return $result
}
