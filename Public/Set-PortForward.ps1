function Set-PortForward {
    <#
    .SYNOPSIS
    Update an existing port forward rule.

    .LINK
    Get-PortForward

    .LINK
    Add-PortForward

    .LINK
    Remove-PortForward
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Id,

        [ValidateSet("wan", "wan2", "both")]
        [string]$Interface,

        [string]$Forward
    )

    $portForward = Get-PortForward | Where-Object { $_._id -eq $Id }
    if ($portForward) {
        $doUpdate = $false
        $body = @{}

        if ($Interface) {
            Write-Debug "Update interface to $Interface"
            $body.pfwd_interface = $Interface
            $doUpdate = $true
        }

        if ($Forward) {
            $body.fwd = $Forward
            $doUpdate = $true
        }

        if ($doUpdate) {
            return Invoke-PutUdmPro -Path "rest/portforward/$Id" -Body $body
        }
    } else {
        throw "Portforward $Id not found"
    }

    <#
    {
        "site_id": "5fccc4bf85cdae03a25a3cbc",
        "_id": "62fccfd9a93a3755e294989e",
        "pfwd_interface": "both",
        "fwd": "10.116.12.187",
        "fwd_port": "22",
        "proto": "tcp",
        "src": "any",
        "name": "joinbox01-ssh",
        "destination_ip": "any"
        "dst_port": "8223",
        "enabled": true,
        "log": false
    }

    {
        "site_id": "5fccc4bf85cdae03a25a3cbc",
        "_id": "5fcd2e44c06ac1066efc646f",
        "pfwd_interface": "wan",
        "fwd": "10.116.11.108",
        "fwd_port": "80",
        "proto": "tcp",
        "src": "any",
        "name": "http to proxy02",
        "destination_ip": "any",
        "dst_port": "80",
        "enabled": true
    }
    #>
}
