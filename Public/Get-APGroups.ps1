function Get-APGroups {
    <#
    .SYNOPSIS
    Get Networks or VLANs.

    .LINK
    Add-WiFiNetwork
    #>

    return Invoke-GetUdmPro -Path "apgroups"
}
