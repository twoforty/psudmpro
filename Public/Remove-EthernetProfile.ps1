function Remove-EthernetProfile {
    <#
    .SYNOPSIS
    Remove ethernet profile.

    .LINK
    Get-EthernetProfile

    .LINK
    Add-FirewallGroup
    #>

    [CmdletBinding(DefaultParameterSetName="id", SupportsShouldProcess)]
    param (
        [Parameter(ParameterSetName="id", ValueFromPipeline)]
        [string]$Id,

        [Parameter(ParameterSetName="name")]
        [string]$Name,

        [Parameter(ParameterSetName="pscustomobject", ValueFromPipeline)]
        [PSCustomObject]$Object
    )

    switch($PSCmdlet.ParameterSetName) {
        "pscustomobject" {
            if ($Object.PSObject.Properties.name -contains("_id")) {
                $Id = $Object._id
            }
        }

        "name" {
            $result = Get-EthernetProfile
            $item = $result | Where-Object { $_.name -eq $Name }
            if (!$item) {
                throw "EthernetProfile not found"
            }
            $Id = $item._id
        }
    }

    if ($PSCmdlet.ShouldProcess($Id, "Remove EthernetProfile")) {
        return Invoke-DeleteUdmPro -Path "rest/portconf/$Id"
    }
}

