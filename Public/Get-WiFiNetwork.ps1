function Get-WiFiNetwork {
    <#
    .SYNOPSIS
    Get WiFi networks.

    .LINK
    Add-WiFiNetwork

    .LINK
    Set-WiFiNetwork

    .LINK
    Remove-WiFiNetwork
    #>

    [CmdletBinding()]
    param ()

    return Invoke-GetUdmPro -Path "rest/wlanconf"
}
