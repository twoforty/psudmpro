function Remove-TrafficRule {
    <#
    .SYNOPSIS
    Removes a traffic rule.

    .LINK
    Get-TrafficRule
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string]$Id
    )

    $path = "trafficrules/$Id"
    Write-Debug "Remove-TrafficRule: $path"
    Invoke-DeleteUdmPro -Path $path
}
