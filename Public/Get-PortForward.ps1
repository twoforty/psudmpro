function Get-PortForward {
    <#
    .SYNOPSIS
    Get port forward rules.

    .LINK
    Add-PortForward

    .LINK
    Set-PortForward

    .LINK
    Remove-PortForward
    #>

    return Invoke-GetUdmPro -Path "rest/portforward"
}
