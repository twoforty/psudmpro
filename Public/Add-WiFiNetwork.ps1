function Add-WiFiNetwork {
    <#
    .SYNOPSIS
    Adds a new WiFi network.

    .LINK
    Get-WiFiNetwork

    .LINK
    Remove-WiFiNetwork

    .LINK
    Set-WiFiNetwork
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Name,

        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Password,

        [ValidateNotNullOrWhiteSpace()]
        [ValidateSet("2g", "5g", "6g")]
        [string[]]$Bands = @("2g", "5g", "6g"),

        # NetworkId is the unique _id of the network. Mutually exclusive with NetworkName and VlanId
        [string]$NetworkId,
        # NetworkName is the Name of the network. Mutually exclusive with NetworkId and VlanId.
        [string]$NetworkName,
        # VlanId is the VLAN id of the network. Mutually exclusive with NetworkName and NetworkId.
        [int]$VlanId = -1,

        [string]$APGgroupIds,
        [string]$APGroupMode = "all",

        # BSS Transition check box
        [switch]$NoBSSTransition,

        # Fast Roaming check box
        [switch]$FastRoaming,

        # Hide WiFi Name check box
        [switch]$HideWiFiName,
        [switch]$NoBandSteering,
        [switch]$HotspotPortal,
        [switch]$ClientDeviceIsolation,
        [switch]$MulticastEnhancement,

        [ValidateNotNullOrWhiteSpace()]
        [ValidateSet("required", "optional")]
        [string[]]$PMFMode = "optional",

        [ValidateSet("ccmp")]
        [string]$WPAEncryption = "ccmp",
        [ValidateSet("wpa2")]
        [string]$WPAMode = "wpa2",
        [ValidateSet("wpapsk")]
        [string]$Security = "wpapsk",

        [int]$SAEAntiClogging = 5,
        [int]$SAESync = 5,

        [string]$UserGroupId,

        [switch]$Disabled
    )

    if ($null -eq $APGgroupIds) {
        $apGroups = Get-APGroups
        $apGroup = $apGroups | Where-Object { $_.name -eq "All APs" }
        if ($null -eq $apGroup) {
            throw "Unable to detect AP group, specify APGgroupIds"
        }
        $APGgroupIds = $apGroup._id
    }

    if ($null -eq $UserGroupId) {
        $userGroups = Get-UserGroup
        if ($userGroups.Count -ne 1) {
            throw "Unable to determine user group, specify UserGroupId"
        }
        $UserGroupId = ($userGroups | Select-Object -First 1)._id
    }

    if ($NetworkId) {
        # NOOP
    } elseif ($NetworkName) {
        # Lookup NetworkId for NetworkName
        $network = Get-Network | Where-Object { $_.name -eq $NetworkName }
        if ($network) {
            $NetworkId = $network._id
        } else {ß
            throw "No network with name $NetworkName found"
        }
    } elseif ($VlanId -ge 0) {
        # Lookup Networkid for VlanId
        $network = Get-Network | Where-Object { $_.vlan -eq $VlanId }
        if ($network) {
            $NetworkId = $network._id
        } else {
            throw "No network with VLAN Id $VlanId found"
        }
    } else {
        throw "No network specified"
    }

    $body = @{
        # Name
        name = $Name

        # Password
        x_passphrase = $Password

        # Broadcasting AP's => get ID's from Get-APGroups
        ap_group_ids = @($APGgroupIds)
        # Broadcasting AP's => "all", "specific", "groups"
        ap_group_mode = $APGroupMode

        bc_filter_list = @()

        # Network / VLAN => must be _id from an existing VLAN via Get-Network
        networkconf_id = $NetworkId

        # Unclear what this is for.
        usergroup_id = $UserGroupId

        # BSS Transition => default true
        bss_transition = -not $NoBSSTransition
        dtim_mode = "default"

        # Enabled => default true
        enabled = -not $Disabled

        # fast Roamin => default false
        fast_roaming_enabled = $FastRoaming

        # Group Rekey Interval => default disabled, 0
        group_rekey = 0

        # Hide WiFi Name => default false
        hide_ssid = $HideWiFiName

        # Is this "Band Steering"? => default true
        iapp_enabled = -not $NoBandSteering

        # Is this "Hotspot Portal"? => default false
        is_guest = $HotspotPortal

        # Is this "Client Device Isolation"? => default false
        l2_isolation = $ClientDeviceIsolation

        mac_filter_enabled = false
        mac_filter_list = @()
        mac_filter_policy = "allow"

        # Multicast Enhancement
        mcastenhance_enabled = $MulticastEnhancement

        minrate_na_advertising_rates = false
        minrate_na_data_rate_kbps = 6000
        minrate_na_enabled = false
        minrate_ng_advertising_rates = false
        minrate_ng_data_rate_kbps = 1000
        minrate_ng_enabled = true
        minrate_setting_preference = "auto"
        no2ghz_oui = true

        # PMF
        #   WPA2/WPA3 => Required/Optional
        #   WPA3 => Required
        pmf_mode = $PMFMode

        private_preshared_keys = @()
        private_preshared_keys_enabled = false

        proxy_arp = false
        radius_das_enabled = false
        radius_mac_auth_enabled = false
        radius_macacl_format = "none_lower"
        sae_groups = @()
        sae_psk = @()
        schedule = @()
        schedule_with_duration = @()
        security = $Security
        setting_preference = "auto"
        uapsd_enabled = false
        wlan_bands = $Bands
        wpa3_fast_roaming = false
        wpa3_support = true
        wpa3_transition = true
        wpa_enc = $WPAEncryption
        wpa_mode = $WPAMode
        sae_anti_clogging = $SAEAntiClogging
        sae_sync = $SAESync
    }

    return Invoke-PostUdmPro -PathV2 "rest/wlanconf" -Body $body
}

# See config_dumps/wlanconf.jsonc
