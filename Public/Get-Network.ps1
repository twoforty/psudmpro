function Get-Network {
    <#
    .SYNOPSIS
    Get Virtual Networks or VLANs.

    .LINK
    Add-Network

    .LINK
    Set-Network

    .LINK
    Remove-Network
    #>

    [CmdletBinding()]
    param (
        [string]$Id,

        [string]$Name,

        [string]$Purpose
    )

    Write-Debug "Entry Get-Network"
    $url = "rest/networkconf"
    if ($Id) {
        $url += "/$Id"
    }
    $result = Invoke-GetUdmPro -Path $url
    if ($Name) {
        $result = $result | Where-Object { $_.name -like $Name }
    }
    if ($Purpose) {
        $result = $result | Where-Object { $_.purpose -like $Purpose }
    }
    Write-Debug "Exit Get-Network"
    return $result
}
