function Get-FirewallGroup {
    <#
    .SYNOPSIS
    Get firewall or IP groups.

    .LINK
    Add-FirewallGroup

    .LINK
    Set-FirewallGroup

    .LINK
    Remove-FirewallGroup
    #>

    [CmdletBinding(DefaultParameterSetName="None")]
    param (
        [Parameter(ParameterSetName="id")]
        [string]$Id,

        [Parameter(ParameterSetName="name")]
        [string]$Name
    )

    switch($PSCmdlet.ParameterSetName) {
        "id" {
            $result = Invoke-GetUdmPro -Path "rest/firewallgroup/$Id"
        }
        { "name", "none" -contains $_ } {
            $result = Invoke-GetUdmPro -Path "rest/firewallgroup"
            if ($Name) {
                $result = $result | Where-Object { $_.name -like $Name }
            }
        }
    }

    return $result
}

