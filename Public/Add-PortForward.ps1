function Add-PortForward {
    <#
    .SYNOPSIS
    Add a new port forwarding rule.

    .LINK
    Get-PortForward

    .LINK
    Set-PortForward

    .LINK
    Remove-PortForward
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Name,

        [ValidateSet("wan", "wan2", "both")]
        [string]$Interface = "both",

        [ValidateNotNullOrWhiteSpace()]
        [string]$Source = "any",

        [String]$Port,

        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Forward,

        [int]$ForwardPort,

        [ValidateSet("tcp", "udp", "both")]
        [string]$Protocol = "both",

        [switch]$Disabled,
        [switch]$Log
    )

    $body = @{
        name = $Name
        enabled = -not $Disabled.IsPresent
        pfwd_interface = $Interface
        src = $Source
        dst_port = $Port
        fwd = $Forward
        fwd_port = $ForwardPort
        proto = $(if ($Protocol -eq "both") { "tcp_udp" } else { $Protocol })
        log = $Log.IsPresent
        destination_ip = "any"
    }
    return Invoke-PostUdmPro -Path "rest/portforward" -Body $body
}

<#
POST https://<IP>/proxy/network/api/s/default/rest/portforward
{
    "name": "TEST",
    "enabled": true,
    "pfwd_interface": "wan",
    "src": "any",
    "dst_port": "1234",
    "fwd": "127.0.0.1",
    "fwd_port": "1234",
    "proto": "tcp_udp",
    "log": false,
    "destination_ip": "any"
}
#>
