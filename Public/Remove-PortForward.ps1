function Remove-PortForward {
    <#
    .SYNOPSIS
    Removes a Port Forwarding rule.

    .LINK
    Get-PortForward

    .LINK
    Add-PortForward

    .LINK
    Set-PortForward
    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string]$Id
    )

    Invoke-DeleteUdmPro -Path "rest/portforward/$Id"
}
