function Get-UserSelf {
    <#
    .SYNOPSIS
    Get logged in user info.
    #>

    [CmdletBinding()]
    param ()

    Invoke-GetUdmPro -Path "api/users/self"
}
