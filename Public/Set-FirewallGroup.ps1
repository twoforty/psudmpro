function Set-FirewallGroup {
    <#
    .SYNOPSIS
    Update a firewall or IP group.

    .LINK
    Get-FirewallGroup

    .LINK
    Add-FirewallGroup

    .LINK
    Remove-FirewallGroup
    #>

    [CmdletBinding(DefaultParameterSetName="name")]
    param (
        [Parameter(ParameterSetName="id", Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Id,

        [Parameter(ParameterSetName="name", Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string]$Name,

        [Parameter(Mandatory)]
        [ValidateNotNullOrWhiteSpace()]
        [string[]]$Members
    )

    $fwg = $null
    switch ($PSCmdlet.ParameterSetName) {
        "id" {
            $fwg = Get-FirewallGroup -Id $Id
        }

        "name" {
            $fwg = Get-FirewallGroup -Name $Name
        }
    }
    if (!$fwg) {
        throw "FirewallGroup not found"
    }
    $Id = $fwg._id

    $fwg.group_members = $Members

    return Invoke-PutUdmPro -Path "rest/firewallgroup/$Id" -Body $fwg
}

