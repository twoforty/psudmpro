function Get-RadiusUsers {
    <#
    .SYNOPSIS
    Get Radius users.
    #>

    return Invoke-GetUdmPro -Path "radius/users"
}
