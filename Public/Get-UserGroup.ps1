function Get-UserGroup {
    <#
    .SYNOPSIS
    Get user groups.
    #>

    [CmdletBinding()]
    param ()

    return Invoke-GetUdmPro -Path "rest/usergroup"
}
