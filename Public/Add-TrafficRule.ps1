function Add-TrafficRule {
    <#
    .SYNOPSIS
    Adds a new traffic rule.

    .LINK
    Get-TrafficRule

    .LINK
    Remove-TrafficRule

    .LINK
    New-TrafficRuleIPAddressEntry
    #>
    [CmdletBinding()]
    param (
        [ValidateNotNullOrWhiteSpace()]
        [ValidateSet('ALLOW', 'BLOCK')]
        [string]$Action = "BLOCK",

        [string]$Description,

        [ValidateNotNullOrWhiteSpace()]
        [ValidateSet('IP')]
        [string]$MatchingTarget = "IP",

        [string]$TargetType = "CLIENT",
        [string]$TargetMac,

        # List of IPAddresses. Use New-TrafficRuleIPAddressEntry to create an entry.
        [object[]]$IPAddresses,

        # List or IP ranges. Each entry is an object with a Start, Stop and Version property.
        [object[]]$IPRanges,

        [switch]$Disabled
    )

    $body = @{
        enabled = -not $Disabled.IsPresent
        description = $Description
        action = $Action
        domains = @()
        bandwidth_limit = @{
            enabled = $false
            download_limit_kbps = 1024
            upload_limit_kbps = 1024
        }
        matching_target = $MatchingTarget
        app_ids = @()
        app_category_ids = @()
        target_devices = @(
            @{
                type = $TargetType
                client_mac = $TargetMac
            }
        )
        network_ids = @()
        regions = @()
        schedule = @{
            mode = "ALWAYS"
            repeat_on_days = @()
            date_start = (Get-Date).ToString("yyyy-MM-dd")
            date_end = ((Get-Date).AddDays(7)).ToString("yyyy-MM-dd")
            time_range_start = "09:00"
            time_range_end = "12:00"
        }
        ip_addresses = $IPAddresses
        ip_ranges = @()
    }

    return Invoke-PostUdmPro -Path "trafficrules" -Body $body
}
