function Get-Site {
    <#
    .SYNOPSIS
    Get available sites.
    #>

    [CmdletBinding()]
    param (
        [string]$Name
    )

    Write-Debug "Entry Get-Site"
    $result = Invoke-GetUdmPro -Path "proxy/network/api/self/sites"
    if ($Name) {
        $result = $result | Where-Object { $_.name -eq $Name }
    }
    Write-Debug "Exit Get-Site"
    return $result
}
