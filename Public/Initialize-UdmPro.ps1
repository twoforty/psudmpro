function Initialize-UdmPro {
    <#
    .SYNOPSIS
    Initializes the module for a connection to a Unifi UdmPro.
    #>

    param (
        [Parameter(Mandatory)]
        [string]$Url,

        [string]$Site = "default",

        [switch]$SkipCertificateCheck
    )

    $Script:baseUrl = $Url
    $Script:SkipCertificateCheck = $SkipCertificateCheck.IsPresent
    if ($null -eq ([System.Uri]$Script:baseUrl).Host) {
        throw [System.ArgumentException] "Could not get host from $Url"
    }
    $Script:siteName = $Site
    # TODO: During connect, check if Site name exists.
}
