#Requires -PSEdition Core
#Requires -Version 6

# Get public and private function definition files.
$Public  = @( Get-ChildItem -Path $PSScriptRoot\Public\*.ps1 -ErrorAction SilentlyContinue )
$Private = @( Get-ChildItem -Path $PSScriptRoot\Private\*.ps1 -ErrorAction SilentlyContinue )

# Module wide parameters
$Script:baseUrl = $null
$Script:SkipCertificateCheck = $false
$Script:userSelf = $null
$Script:webSession = $null
$Script:token = $null

# Dot source the files
foreach($import in @($Public + $Private))
{
    try
    {
        . $import.fullname
    }
    catch
    {
        Write-Error -Message "Failed to import function $($import.fullname): $_"
    }
}

$ErrorActionPreference = "Stop";
Set-StrictMode -Version Latest;

foreach($import in $Public) {
    Export-ModuleMember -Function $import.Basename;
}
