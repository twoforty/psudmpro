# PowerShell interface to Ubiquiti UniFi Dream Machine Pro (UDM Pro)

This PowerShell module should be named differently, since it interfaces with the Unifi Network Application API.

## Example

1. Import this module.
1. Initialize the module by giving it connection information.
1. Connect to the UDM Pro.
1. Interact with the API.

```Powershell
Import-Module PSUdmPro

Initialize-UdmPro -Url "https://192.168.0.1" -SkipCertificateCheck
Connect-UdmPro

Get-NetworkSettings
```

## Progress

Development is done on the following device:

- UniFi Dream Machine Pro
- Model: UDM Pro
- Processor: ARM 64-bit 4 cores
- UniFi OS: v3.2.12
- Network Application
- Version: v8.1.113

- [X] Connect / Login
  - [X] Initialize-UdmPro
  - [X] Connect-UdmPro
- [ ] Sites
  - [ ] Select / Activate Site
  - [X] Get-Site
- [ ] WiFi
  - [X] Get-WiFiNetwork
  - [X] Add-WiFiNetwork
  - [ ] Set-WiFiNetwork
  - [X] Remove-WiFiNetwork
  - [X] Get-APGroups
  - [ ] Radios / Radio Manager
  - [ ] Channelization / Optimize Now
- [ ] Networks
  - [ ] VLANs
    - [X] Get-Network
    - [X] Add-Network
    - [ ] Set-Network
    - [X] Remove-Network
    - [X] Add-AutoScaleNetwork
  - [ ] Global Network Settings
    - [X] Get-NetworkSettings
    - [X] Set-NetworkSettings
    - [ ] Switch Isolation Settings
    - [ ] Global Switch Settings
- [ ] Internet
- [ ] VPN
  - [ ] Teleport
  - [ ] VPN Server
  - [ ] VPN Client
  - [ ] Site-to-Site VPN
- [ ] Security
  - [ ] General
    - [ ] Internal Honeypot
    - [ ] Security Detection Allow List
  - [X] Port Forwarding
    - [X] Get-PortForward
    - [X] Add-PortForward
    - [X] Set-PortForward
    - [X] Remove-PortForward
  - [ ] Traffic & Firewall Rules
    - [X] Traffic Rules
      - [X] Get-TrafficRule
      - [X] Add-TrafficRule
      - [ ] Set-TrafficRule
      - [X] Remove-TrafficRule
    - [ ] Firewall Rules
- [ ] Routing
  - [ ] Policy-Based Routes
  - [ ] Static Routes
  - [ ] OSPF
- [ ] Profiles
  - [ ] Ethernet Ports
    - [X] Get-EthernetProfile
    - [X] Add-EthernetProfile
    - [ ] Set-EthernetProfile
    - [X] Remove-EthernetProfile
  - [ ] WiFi Speed Limit
  - [ ] RADIUS
    - [X] Get-RadiusUsers
  - [X] IP Groups
    - [X] Get-FirewallGoup
    - [X] Add-FirewallGoup
    - [X] Set-FirewallGoup
    - [X] Remove-FirewallGroup
- [ ] Devices
  - [X] Get-Device
- [ ] System
  - [ ] General
  - [ ] Updates
  - [ ] Backups
    - [ ] Get-Backup
  - [ ] Advanced
- [ ] Users & Groups
  - [X] Get-UserSelf
  - [X] Get-UserGroup

## TODO Ansible

When PSUdmPro has basic functionality, I'll start working on Ansible module(s) for the Udm Pro.

## See also

- [Unifi API](https://dl.ui.com/unifi/8.1.113/unifi_sh_api): `https://dl.ui.com/unifi/<version>/unifi_sh_api`
- [Unifi-API-client](https://github.com/Art-of-WiFi/UniFi-API-client/)
