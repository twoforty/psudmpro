function Invoke-PostUdmPro {
    <#
    .SYNOPSIS
    Invokes a POST call.
    #>
    [CmdletBinding()]
    param (
        [string]$Path,
        [string]$QueryString,
        [object]$Body
    )

    Invoke-MethodUdmPro -Method Post -Path $Path -QueryString $QueryString -Body $Body
}
