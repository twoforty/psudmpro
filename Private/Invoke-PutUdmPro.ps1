function Invoke-PutUdmPro {
    <#
    .SYNOPSIS
    Invokes a PUT call.
    #>
    [CmdletBinding()]
    param (
        [string]$Path,
        [string]$QueryString,
        [object]$Body
    )

    Invoke-MethodUdmPro -Method Put -Path $Path -QueryString $QueryString -Body $Body
}

