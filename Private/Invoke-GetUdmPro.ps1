function Invoke-GetUdmPro {
    <#
    .SYNOPSIS
    Invokes a GET call.
    #>

    [CmdletBinding()]
    param (
        [string]$Path,

        [string]$QueryString
    )

    Write-Debug "Invoking Invoke-MethodUdmPro $Path"
    return Invoke-MethodUdmPro -Method Get -Path $Path -QueryString $QueryString
}
