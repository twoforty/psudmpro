function Invoke-MethodUdmPro {
    <#
    .SYNOPSIS
    Generic method for invoking a REST api call.
    #>

    [CmdletBinding()]
    param (
        [Microsoft.PowerShell.Commands.WebRequestMethod]$Method = [Microsoft.PowerShell.Commands.WebRequestMethod]::GET,
        [string]$Path,
        [string]$QueryString,
        [object]$Body
    )

    $urlMapping = @{
        "rest/wlanconf" = "v1"
        "rest/networkconf" = "v1"
        "rest/portconf" = "v1"
        "rest/portforward" = "v1"
        "rest/usergroup" = "v1"
        "rest/firewallgroup" = "v1"
        "stat/widget/subnet-suggest" = "v1"
        "stat/device-basic" = "v1"
        "cmd/backup" = "v1"
        "trafficrules" = "v2"
        "apgroups" = "v2"
        "global/config/network" = "v2"
        "radius/users" = "v2"
    }
    $apiVersion = ""
    foreach($key in $urlMapping.Keys) {
        if ($Path.StartsWith($key)) {
            $apiVersion = $urlMapping[$key]
            break
        }
    }
    switch ($apiVersion) {
        "v1" {
            $urlPath = "proxy/network/api/s/$Script:siteName/" + $Path.TrimStart("/")
        }

        "v2" {
            $urlPath = "proxy/network/v2/api/site/$Script:siteName/" + $Path.TrimStart("/")
        }

        default {
            $urlPath = $Path
        }
    }

    $url = ("{0}/{1}" -f $Script:baseUrl.Trim("/"), $urlPath.TrimStart("/"))
    if ($QueryString) {
        $url += "?$QueryString"
    }
    Write-Debug "Invoke-MethodUdmPro Uri=$url, Method=$Method"
    $arguments = @{
        Uri = $url
        Method = $Method
        ContentType = "application/json"
        SkipCertificateCheck = $Script:SkipCertificateCheck
        WebSession = $Script:webSession
    }
    if ($Body) {
        $arguments.Body = ($Body | ConvertTo-Json -Depth 10)
        # Out-File -FilePath .\out.json -InputObject $arguments.Body
    }
    if ($Method -ne 'Get') {
        Write-Debug "Method is not Get, adding x-csrf-token"
        $arguments.Headers = @{
            "x-csrf-token" = $Script:tokenJsonPart.csrfToken
        }
    }
    $result = Invoke-RestMethod @arguments
    # TODO In case of GET method:
    # TODO: When the REST endpoint returns multiple objects, the objects are received as an array.
    #       If you pipe the output from Invoke-RestMethod to another command, it is sent as a single [Object[]] object.
    #       The contents of that array are not enumerated for the next command on the pipeline.
    # TODO: Maybe we should iterate here and yield the results.

    if ($result ) {
        if ($result -is [PSCustomObject]) {
            if ($result.PSObject.Properties.name -contains "meta" -and $result.PSObject.Properties.name -contains "data") {
                return $result.data
            }
        }
        # if ($result -is [hashtable]) {
        #     if ($result.PSObject.Properties.name -contains("meta") -and PSObject.Properties.name -contains("data")) {
        #         return $result.data
        #     }
        # }

        return $result
    }
    # if ($result -and $result.Keys -contains("meta")) {
    #     return $result.data
    # } else {
    #     return $result
    # }
}
