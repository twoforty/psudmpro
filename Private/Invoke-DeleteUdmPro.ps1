function Invoke-DeleteUdmPro {
    <#
    .SYNOPSIS
    Invokes a DELETE call.
    #>

    [CmdletBinding()]
    param (
        [string]$Path,
        [string]$QueryString,
        [object]$Body
    )

    Invoke-MethodUdmPro -Method Delete -Path $Path -QueryString $QueryString -Body $Body
}
